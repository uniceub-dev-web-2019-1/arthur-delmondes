package structs

type Product struct {
	//ID          int     `json:"id"`
	Name        string  `json:"name" validate:"required"`
	Price       float32 `json:"price" validate:"required"`
	Description string  `json:"description"`
	Amount      int     `json:"amount" `
}

type UpdatableProduct struct {
	OldProduct Product `json:"oldProduct" validate:"required"`
	NewProduct Product `json:"newProduct" validate:"required"`
}
