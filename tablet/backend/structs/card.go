package structs

type Card struct {
	Owner  string  `json:"owner" validate:"required"`
	Table  Table   `json:"table" validate:"required"`
	Orders []Order `json:"orders"`
	Closed bool    `json:"closed" validate:"required"`
}

type InsertOrderToCard struct {
}

func (c Card) CardValue() float32 {

	var total float32

	for _, order := range c.Orders {
		total += order.productsPrice()

	}
	return total
}
