import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Admin from '../components/Admin';
import Client from '../components/Client';
import '../App.css';
import App from '../App';
import * as Consts from '../consts/consts'
import CardsRequests from '../components/CardsRequests'

class Routes extends Component {

  constructor() {
    super()
    this.state = {
      table: false,
      haveToRegisterTable: false,
    }
  }

  componentDidMount() {
    const table = localStorage.getItem(Consts.TABLE_NUMBER)
    if (table === null) {
      this.setState({ haveToRegisterTable: true })
    } else {
      this.setState({ table })
    }
  }

  salvarMesa(numero) {
    localStorage.setItem(Consts.TABLE_NUMBER, numero)
    this.setState({ haveToRegisterTable: false })
  }


  render() {
    if (!this.state.haveToRegisterTable) {
      return (
        <Router>
          <Route exact path="/" component={App} />
          <Route path="/admin" component={Admin} />
          <Route path="/cards" component={CardsRequests} />
          <Route path="/client" component={Client} />
        </Router>
      );
    }
    return (
      <div className="App" >
        <form className="Center" >
          <TextField
            onChange={(event) => this.setState({ table: event.target.value })}
            id="standard-password-input"
            label="Mesa"
            type="number"
            margin="normal"
          />
          <br />
          <Button onClick={() => this.salvarMesa(this.state.table)} variant="contained" color="primary" >CADASTRAR</Button>
        </form>
      </div>
    )
  }
}

export default Routes;
