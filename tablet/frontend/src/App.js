import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class App extends Component {

    render() {
        return (
            <div className="App" >
                <div className="Center" >
                    <Link to={"/admin"}>Modo administrador</Link>
                </div>
                <div className="Center" >
                    <Link to={"/client"}>Modo cliente</Link>
                </div>
            </div>
        )
    }
}

export default App;
