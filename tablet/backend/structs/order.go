package structs

import (
	"time"
)

type Order struct {
	Table     int           `json:"table" validate:"required"`
	Time      time.Duration `json:"time" validate:"required"`
	Products  []Product     `json:"products" validate:"required"`
	Delivered bool          `json:"delivered" validate:"required"`
}

func (o Order) productsPrice() float32 {

	var total float32

	for _, product := range o.Products {
		total += product.Price

	}
	return total
}
