package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"tablet/backend/database"
	"tablet/backend/structs"
)

func RegisterTable(resp http.ResponseWriter, req *http.Request) {
	body := req.Body

	var table structs.Table

	bytes, _ := ioutil.ReadAll(body)

	err := json.Unmarshal(bytes, &table)

	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	err = database.RegisterNewTable(table)

	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.Write([]byte(`{"message": "success"}`))

}

func UpdateTable(resp http.ResponseWriter, req *http.Request) {
	//body := req.Body
}
