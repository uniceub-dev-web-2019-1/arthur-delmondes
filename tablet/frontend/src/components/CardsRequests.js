import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import '../App.css';
import * as API from "../api/Api"
import * as Consts from "../consts/consts"

class CardsRequests extends Component {

    constructor() {
        super()
        this.state = {
            comanda: false,
            comandaReady: false,
        }
    }

    componentDidMount() {
        //localStorage.removeItem(Consts.TABLE_NUMBER)
    }


    saveCard(cardName) {
        const tableNumber = parseInt(localStorage.getItem(Consts.TABLE_NUMBER))
        let card = {
            owner: cardName,
            table: { number: tableNumber, available: true },
            orders: []
        }
        API.registerCard(card).then(response => this.setState({ comandaReady: response }))
    }

    
    backToRequests(){
        this.setState({ comandaReady: false, comanda: false })
    }

    renderResponse(response) {
        return (
            <div className="App" >
                Response obtido do servidor:
                <br />
                {response}
                <br />
                <Button onClick={() => this.backToRequests()} variant="contained" color="primary" >VOLTAR</Button>
            </div>

        )
    }


    renderRequests() {
        return (
            <div className="App" >
                -Testar request inserir comanda:
                <div className="Center" >
                    <TextField
                        onChange={(event) => this.setState({ comanda: event.target.value })}
                        id="standard-password-input"
                        label="Nome da comanda"
                        type="text"
                        margin="normal"
                    />
                    <br />
                    <Button onClick={() => this.saveCard(this.state.comanda)} variant="contained" color="primary" >TESTAR REQUEST</Button>
                </div>
            </div>
        )
    }


    render() {
        return (
            this.renderRequests()
        )
    }
}

export default CardsRequests;
