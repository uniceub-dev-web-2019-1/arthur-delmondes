package database

import (
	"context"
	"log"
	"time"

	"tablet/backend/consts"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var Client *mongo.Client

var Database *mongo.Database

func CreateClient() {

	Client, err := mongo.NewClient(options.Client().ApplyURI(consts.MONGO_HOST))

	if err != nil {
		log.Println("[FATAL]  could not create client for database")
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)

	err = Client.Connect(ctx)

	defer cancel()

	if err != nil {
		log.Println("[FATAL] could not connect to database")
		panic(err)
	}

	Database = Client.Database("tablet")

	return
}
