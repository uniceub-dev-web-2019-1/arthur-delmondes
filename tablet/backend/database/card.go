package database

import (
	"context"
	"log"

	"tablet/backend/structs"
)

func InsertCard(card structs.Card) (err error) {

	c := Database.Collection("cards")

	res, err := c.InsertOne(context.TODO(), card)

	if err != nil {
		log.Printf("[ERROR] probleming inserting user: %v %v", err, res.InsertedID)
		return
	}

	return
}
