package util

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"tablet/backend/structs"
)

func HandleJson(struc *interface{}, req http.Request) {

	body := req.Body

	var table structs.Table

	bytes, _ := ioutil.ReadAll(body)

	err := json.Unmarshal(bytes, &table)

	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		//resp.WriteHeader(http.StatusBadRequest)
		return
	}

}
