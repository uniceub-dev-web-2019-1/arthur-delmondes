package consts

const (
	SERVER_ADDR = "localhost:8080"
	MONGO_HOST  = "mongodb://localhost:27017"
)
const (
	TABLE_PATH   = "/tables"
	ORDER_PATH   = "/orders"
	CARD_PATH    = "/cards"
	PRODUCT_PATH = "/products"
)

const (
	CONTENT_TYPE = "Content-Type"
)

const (
	APPLICATION_JSON = "application/json"
)
