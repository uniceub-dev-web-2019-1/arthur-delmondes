package database

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson"

	"tablet/backend/structs"
)

func InsertProduct(product structs.Product) (err error) {

	c := Database.Collection("products")

	res, err := c.InsertOne(context.TODO(), product)

	if err != nil {
		log.Printf("[ERROR] probleming inserting product: %v %v", err, res.InsertedID)
		return
	}

	return
}

func UpdateProduct(product structs.UpdatableProduct) (err error) {

	c := Database.Collection("products")

	// create filter
	filter := bson.D{{"name", product.OldProduct.Name}, {"price", product.OldProduct.Price}, {"description", product.OldProduct.Description}, {"amount", product.OldProduct.Amount}}

	newProduct := bson.D{{"$set", bson.D{{"name", product.NewProduct.Name}, {"price", product.NewProduct.Price}, {"description", product.NewProduct.Description}, {"amount", product.NewProduct.Amount}}}}

	uRes, err := c.UpdateOne(context.TODO(), filter, newProduct)

	if err != nil {
		log.Printf("[ERROR] probleming updating product: %v %v", err, uRes)
	}

	return
}

func GetProducts() (products []structs.Product) {
	c := Database.Collection("products")

	//filter := bson.D{{"$set", bson.D{{structs.Product.Name}, {"price", p.Price}, {"description", p.Description}, {"amount", p.Amount}}}}

	ctx := context.TODO()

	cur, err := c.Find(ctx, bson.D{})

	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var result structs.Product
		err := cur.Decode(&result)
		if err != nil {
			log.Fatal(err)
		}
		products = append(products, result)
	}

	if err != nil {
		log.Printf("[ERROR] probleming finding product: %v ###%v", err, cur)
	}

	return
}
