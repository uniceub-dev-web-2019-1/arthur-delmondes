package service

import (
	"net/http"
	"tablet/backend/consts"
	"tablet/backend/handlers"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func createHandler() (handler *mux.Router) {

	handler = mux.NewRouter()

	handler.HandleFunc(consts.TABLE_PATH, handlers.RegisterTable).Methods(http.MethodPost).Headers(consts.CONTENT_TYPE, consts.APPLICATION_JSON)

	handler.HandleFunc(consts.TABLE_PATH, handlers.UpdateTable).Methods(http.MethodPut).Headers(consts.CONTENT_TYPE, consts.APPLICATION_JSON)

	//handler.HandleFunc(consts.TABLE_PATH, handlers.ReadTable).Methods(http.MethodGet)

	//handler.HandleFunc(consts.TABLE_PATH, handlers.DeleteTable).Methods(http.MethodDelete)

	handler.HandleFunc(consts.PRODUCT_PATH, handlers.RegisterProduct).Methods(http.MethodPost).Headers(consts.CONTENT_TYPE, consts.APPLICATION_JSON)

	handler.HandleFunc(consts.PRODUCT_PATH, handlers.UpdateProduct).Methods(http.MethodPut).Headers(consts.CONTENT_TYPE, consts.APPLICATION_JSON)

	handler.HandleFunc(consts.PRODUCT_PATH, handlers.GetProducts).Methods(http.MethodGet)

	//handler.HandleFunc(consts.PRODUCT_PATH, handlers.DeleteProduct).Methods(http.MethodDelete)

	handler.HandleFunc(consts.ORDER_PATH, handlers.RegisterOrder).Methods(http.MethodPost).Headers(consts.CONTENT_TYPE, consts.APPLICATION_JSON)

	handler.HandleFunc(consts.ORDER_PATH, handlers.GetOrdersByTable).Methods(http.MethodGet)

	handler.HandleFunc(consts.ORDER_PATH, handlers.UpdateOrder).Methods(http.MethodPut).Headers(consts.CONTENT_TYPE, consts.APPLICATION_JSON)

	//handler.HandleFunc(consts.ORDER_PATH, handlers.ReadOrder).Methods(http.MethodGet)

	//handler.HandleFunc(consts.ORDER_PATH, handlers.DeleteOrder).Methods(http.MethodDelete)

	handler.HandleFunc(consts.CARD_PATH, handlers.RegisterCard).Methods(http.MethodPost).Headers(consts.CONTENT_TYPE, consts.APPLICATION_JSON)

	handler.HandleFunc(consts.CARD_PATH, handlers.UpdateCard).Methods(http.MethodPut).Headers(consts.CONTENT_TYPE, consts.APPLICATION_JSON)

	//handler.HandleFunc(consts.CARD_PATH, handlers.ReadCard).Methods(http.MethodGet)

	//handler.HandleFunc(consts.CARD_PATH, handlers.DeleteCard).Methods(http.MethodDelete)

	return
}

func createCORS() *cors.Cors {

	return cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Content-Type", "Accept", "Content-Length", "Accept-Encoding", "Authorization", "X-CSRF-Token"},
	})
}
