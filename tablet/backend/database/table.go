package database

import (
	"context"
	"log"

	"tablet/backend/structs"
)

func RegisterNewTable(table structs.Table) (err error) {

	c := Database.Collection("tables")

	res, err := c.InsertOne(context.TODO(), table)

	if err != nil {
		log.Printf("[ERROR] probleming inserting user: %v %v", err, res.InsertedID)
		return
	}

	return
}
