import * as Consts from "../consts/consts"



export const registerCard = (card) => {
    return fetch(Consts.URL_CARDS, {
        method: Consts.METHOD_POST,
        body: JSON.stringify(
            card
        ),
        headers: Consts.HEADERS
    })
        .then(response => {
            if (response.ok) {
                return response.text()
            } else {
                return JSON.stringify({ error: response.statusText, errorStatus: response.status })
            }
        })
        .catch(err => alert(err))

}