package database

import (
	"context"
	"log"

	"tablet/backend/structs"

	"go.mongodb.org/mongo-driver/bson"
)

func NewOrder(order structs.Order) (err error) {

	c := Database.Collection("orders")

	res, err := c.InsertOne(context.TODO(), order)

	if err != nil {
		log.Printf("[ERROR] probleming inserting user: %v %v", err, res.InsertedID)
		return
	}

	return
}

func GetOrdersByTable(table int) (ors []structs.Order) {

	orders := Database.Collection("orders")

	ctx := context.TODO()

	cur, err := orders.Find(ctx, bson.D{{"table", table}})

	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var result structs.Order
		err := cur.Decode(&result)
		if err != nil {
			log.Fatal(err)
		}
		ors = append(ors, result)
	}

	if err != nil {
		log.Fatal(err)
	}

	return

}
