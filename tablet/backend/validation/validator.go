package validation

import (
	val "gopkg.in/go-playground/validator.v9"
)

var Validator *val.Validate

func CreateValidator() {

	Validator = val.New()

	//Validator.RegisterValidation("tablet-plan", ValidateTabletPlan)
}

//func ValidateTabletPlan(field val.FieldLevel) bool {

//name := field.Field().String()

//return name == "basic" || name == "default" || name == "premium"
//}
