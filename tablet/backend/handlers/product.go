package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"tablet/backend/database"
	"tablet/backend/structs"
)

func RegisterProduct(resp http.ResponseWriter, req *http.Request) {
	body := req.Body

	var product structs.Product

	bytes, _ := ioutil.ReadAll(body)

	err := json.Unmarshal(bytes, &product)

	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	err = database.InsertProduct(product)

	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.Write([]byte(`{"message": "success"}`))

}

func UpdateProduct(resp http.ResponseWriter, req *http.Request) {

	body := req.Body

	var product structs.UpdatableProduct

	bytes, _ := ioutil.ReadAll(body)

	err := json.Unmarshal(bytes, &product)

	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	err = database.UpdateProduct(product)

	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.Write([]byte(`{"message": "success"}`))

}

func GetProducts(resp http.ResponseWriter, req *http.Request) {

	res := database.GetProducts()

	str := "["

	for _, pro := range res {
		str += fmt.Sprintf("{name: %s, price: %s, description: %s, amount: %s}", pro.Name, pro.Price, pro.Description, pro.Amount)
	}

	str += "]"

	resp.Write([]byte(str))

	//resp.Write([]byte(`{"message": "success"}`))

}
