package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"tablet/backend/database"
	"tablet/backend/structs"
)

func RegisterCard(resp http.ResponseWriter, req *http.Request) {
	body := req.Body

	var card structs.Card

	bytes, _ := ioutil.ReadAll(body)

	err := json.Unmarshal(bytes, &card)

	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	err = database.InsertCard(card)

	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.Write([]byte(`{"message": "success"}`))

}

func UpdateCard(resp http.ResponseWriter, req *http.Request) {
	//body := req.Body
}
