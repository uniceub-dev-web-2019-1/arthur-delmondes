package main

import (
	"tablet/backend/database"
	"tablet/backend/service"
	"fmt"
)

func main() {

	defer service.StopServer()

	database.CreateClient()

	service.StartServer()

	fmt.Printf("servidor rodando")

}
