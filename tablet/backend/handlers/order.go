package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"tablet/backend/database"
	"tablet/backend/structs"
)

func RegisterOrder(resp http.ResponseWriter, req *http.Request) {
	body := req.Body

	var order structs.Order

	bytes, _ := ioutil.ReadAll(body)

	err := json.Unmarshal(bytes, &order)

	if err != nil {

		log.Printf("[WARN] problem parsing json body, because, %v\n", err)
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	err = database.NewOrder(order)

	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.Write([]byte(`{"message": "success"}`))

}

func GetOrdersByTable(resp http.ResponseWriter, req *http.Request) {
	table := req.FormValue("table")

	parsed, err := strconv.Atoi(table)

	if err != nil {
		log.Printf("[ERROR] problem parsing table value to int, %v\n", err)
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	ors := database.GetOrdersByTable(parsed)

	arr, _ := json.Marshal(ors)

	resp.Write(arr)

}

func UpdateOrder(resp http.ResponseWriter, req *http.Request) {
	//body := req.Body
}
